randomwalld
=============

A python script to change the wallpaper on X11 window managers or wayland compositors.

The script can be controlled using dbus calls (TODO: how?)

## Usage
Move the script to your ```$PATH``` and make it executable or run it with ```python randomwalld```.

## Dependencies

- ```dbus-python``` (dbus stuff)
- ```pygobject``` (dbus-python glib mainloop)
- ```feh``` (change wallpaper on X11)
- ```swaybg``` (change wallpaper on wlroots based compositors)